<?php include '_theme-head.php'; ?>
<?php include '_theme-toolbar.php'; ?>

<section class="header row-table" style="height: 300px;">
	<div class="col-cell">
		<div class="container">
			<h1 class="text-700">Spoken Live</h1>
			<h2 class="h5 text-serif">
				Ibrahim Sani - January 20, 2016
			</h2>
			<h5 class="mt-5 text-uppercase">Live Radio</h5>
		</div>
	</div>
</section>

<section class="content container pt-gutter pb-gutter">
	<div class="finder row-table">
		<div class="title col-cell">
			<h2 class="text-700 ma-0">Discover Spoken</h2>
		</div>

		<div class="form col-cell mt-10">
			<div class="form-control-icon" data-toggle="modal" data-target="#modal-search">
				<i class="zmdi zmdi-search pull-right" style="font-size: 20px;"></i>
				<div class="form-control input-lg text-muted">Search for audio clips</div>
			</div>
		</div>
	</div>

	<div class="tab-list-wrapper mt-gutter mb-gutter">
		<ul class="tab-list reset-list text-uppercase clearfix">
			<li class="active">
				<a href="#tab-0"role="tab" data-toggle="tab">Live Radio</a>
			</li>

			<li>
				<a href="#tab-1"data-toggle="tab">Stand Up Comedy</a>
			</li>

			<li>
				<a href="#tab-2"data-toggle="tab">Political</a>
			</li>

			<li>
				<a href="#tab-3"data-toggle="tab">Tech Startup</a>
			</li>

			<li>
				<a href="#tab-4"data-toggle="tab">Literature</a>
			</li>

			<li>
				<a href="#tab-5"data-toggle="tab">Sport</a>
			</li>
		</ul>
	</div>

	<div class="row">
		<div class="tab-content tab-responsive">
			<?php 
				$radio_title  = ["Hitz FM", "Morning Laugh", "Stop Racism", "Programming Syllabus", "Shakespeare & 6 Ghosts", "SuperBowl 2015"];
				$radio_author = ["Eski Mirza", "Ili Farhana", "Ikram Hakimi", "Sudhan", "Tan Choo Zin", "Muslim Nazri"];
				for ($a=0; $a < 6; $a++) { 
			?>
				<!--tab-pane-->
				<div class="tab-pane fade<?php if ($a == 0) { echo " in active"; } ?>" id="tab-<?php echo $a;?>">
					<?php for ($i=0; $i < 12; $i++) { ?>
					<div class="column col-md-3 col-sm-4 mb-gutter">
						<div class="card">
							<div class="cover radius-3" style="background-color: #dadadd; padding-bottom: 75%;"></div>

							<div class="detail text-center mt-15">
								<h3 class="h5 text-uppercase ma-0"><?php echo $radio_title[$a]; ?></h3>
								<div class="text-serif text-muted mt-5"><?php echo $radio_author[$a]; ?></div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<!--tab-pane-->
			<?php } ?>
		</div>
	</div>
</section>


<article class="modal modal-search" id="modal-search" tabindex="-1" role="dialog">

	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			<div class="close text-right pa-20" data-dismiss="modal">
				<i class="zmdi zmdi-close text-muted"></i>
				<b class="text-uppercase text-sm ml-5">Close</b>
			</div>

			<form class="container pb-gutter pt-gutter">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="form-control-icon">
							<i class="zmdi zmdi-search pull-right" style="font-size: 20px;"></i>
							<input class="form-control input-lg" placeholder="Search for audio clips">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</article>
<?php include '_theme-foot.php'; ?>