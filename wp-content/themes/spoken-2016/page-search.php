<?php
/* Template Name: Search */

get_header();

// Search
if (isset($_GET['keyword'])) {

    $args = array(
        'posts_per_page' => 20,
        'post_type' => 'post',
        'search_prod_title' => $_GET['keyword']
    );

    add_filter('posts_where', 'title_filter', 10, 2);

    $shows = new WP_Query($args);

    ?>
    <?php

    if ($shows->have_posts()) {
        $shows->the_post(); ?>
        <div class="column col-md-3 col-sm-4 mb-gutter">
            <div class="card">
                <?php $image = get_field('hero_image'); ?>
                <div class="cover radius-3"
                     style="background: url(<?php echo $image[0]['url']; ?>) no-repeat; background-size: cover; padding-bottom: 75%;"></div>

                <div class="detail text-center mt-15">
                    <h3 class="h5 text-uppercase ma-0"><?php the_title(); ?></h3>
                    <div
                        class="text-serif text-muted mt-5"><?php the_field('author'); ?></div>
                </div>
            </div>
        </div>

        <?php
    }
    wp_reset_query();
    ?>
<?php } ?>