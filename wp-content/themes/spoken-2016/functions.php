<?php

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Popular Searches',
        'menu_title' => 'Popular Searches',
        'menu_slug' => 'popular-searches',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_page(array(
        'page_title' => 'Category Products',
        'menu_title' => 'Category Products',
        'menu_slug' => 'category-products',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

}

date_default_timezone_set('Asia/Kuala_Lumpur');

function spoken_stylesheets()
{
    wp_enqueue_style('theme', get_stylesheet_directory_uri() . '/assets/css/theme.css');
    wp_enqueue_style('icons', get_stylesheet_directory_uri() . '/assets/icons/material/css/material-design-iconic-font.min.css');
}

function spoken_scripts()
{
    wp_deregister_script('jquery');

    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/assets/js/jquery-1.11.3.min.js', array(), null, true);
    wp_enqueue_script('bootstrap.min', get_stylesheet_directory_uri() . '/assets/bootstrap/dist/js/bootstrap.min.js', array(), "1.0", true);
    wp_enqueue_script('scrollbar.min', get_stylesheet_directory_uri() . '/assets/js/jquery.scrollbar.min.js', array(), "1.0", true);
    wp_enqueue_script('bootstrap-datepicker', get_stylesheet_directory_uri() . '/assets/js/bootstrap-datepicker.js', array(), "1.0", true);
    wp_enqueue_script('theme', get_stylesheet_directory_uri() . '/assets/js/theme.js', array(), "1.0", true);
}


function get_domain($url)
{
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }
    return false;
}

global $domain_name;

$url = get_domain("http://" . $_SERVER['HTTP_HOST']);
$url_x = explode(".", $url);
$domain_name = $url_x[0];

add_action('wp_enqueue_scripts', 'spoken_stylesheets');
add_action('wp_enqueue_scripts', 'spoken_scripts');

function title_filter( $where, &$wp_query )
{
    global $wpdb;
    if ( $search_term = $wp_query->get( 'search_prod_title' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $search_term ) ) . '%\'';
    }
    return $where;
}

function getCategories(){
    $categories = get_categories( array( 'hide_empty' => 0 ) ); //remove hide empty
    return $categories;
}

function getShows($category){
    $args = array(
        'posts_per_page'   => 20,
        'offset'           => 0,
        'category_name'    => $category,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish',
    );

    $posts_array = new WP_Query( $args );

    return $posts_array;
}

?>

