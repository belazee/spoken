<?php get_header(); ?>

<?php include 'toolbar.php'; ?>

<section id="hero-image" class="header row-table" style="height: 300px;">
    <div class="col-cell">
        <div class="container">
            <h1 class="text-700" id="show_title">Spoken Live</h1>
            <h2 class="h5 text-serif" id="author">
                Ibrahim Sani - January 20, 2016
            </h2>
            <h5 class="mt-5 text-uppercase" id="category">Live Radio</h5>
        </div>
    </div>
</section>

<section class="content container pt-gutter pb-gutter">
    <div class="finder row-table">
        <div class="title col-cell">
            <h2 class="text-700 ma-0">Discover Spoken</h2>
        </div>

        <div class="form col-cell mt-10">
            <div class="form-control-icon" data-toggle="modal" data-target="#modal-search">
                <i class="zmdi zmdi-search pull-right" style="font-size: 20px;"></i>
                <div class="form-control input-lg text-muted">Search for audio clips</div>
            </div>
        </div>
    </div>

    <div class="tab-list-wrapper mt-gutter mb-gutter">
        <ul class="tab-list reset-list text-uppercase clearfix">
            <?php $categories = getCategories();
            $i = 0;
            foreach ($categories as $category) { ?>
                <li class="<?php echo($i == 0 ? "active" : '') ?>">
                    <a href="#tab-<?php echo $i++; ?>" role="tab"
                       data-toggle="tab"><?php echo $category->name; ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>

    <div class="row">
        <div class="tab-content tab-responsive">
            <?php
            $i = 0;
            foreach ($categories as $category) { ?>
                <!--tab-pane-->
                <div class="tab-pane fade <?php echo($i == 0 ? " in active" : '') ?>" id="tab-<?php echo $i++; ?>">

                    <?php $shows = getShows($category->name); ?>

                    <?php if ($shows->have_posts()) {
                        $shows->the_post();  ?>
                        <div class="column col-md-3 col-sm-4 mb-gutter">
                            <div class="card">
                                <?php $image = get_field('image'); ?>
                                <?php $hero_image = get_field('hero_image'); ?>

                                <div class="cover radius-3"
                                     onclick="selectShow('<?php the_title(); ?>', '<?php the_field('author'); ?> - <?php the_date(); ?>', '<?php echo $category->name; ?>', '<?php the_field('link'); ?>', '<?php echo $hero_image[0]['url']; ?>')"
                                     style="background: url(<?php echo $image[0]['url']; ?>) no-repeat; background-size: cover; padding-bottom: 75%;"></div>

                                <div class="detail text-center mt-15">
                                    <h3 class="h5 text-uppercase ma-0"><?php the_title(); ?></h3>
                                    <div
                                        class="text-serif text-muted mt-5"><?php the_field('author'); ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    wp_reset_query();
                    ?>
                </div>
                <!--tab-pane-->

            <?php } ?>


        </div>
    </div>
</section>


<article class="modal modal-search" id="modal-search" tabindex="-1" role="dialog">

    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="close text-right pa-20" data-dismiss="modal">
                <i class="zmdi zmdi-close text-muted"></i>
                <b class="text-uppercase text-sm ml-5">Close</b>
            </div>

            <form class="container pb-gutter pt-gutter">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="form-control-icon">
                            <i class="zmdi zmdi-search pull-right" style="font-size: 20px;"></i>
                            <input id="search-input" class="form-control input-lg"
                                   placeholder="Search for audio clips"/>
                        </div>
                    </div>
                </div>
            </form>
            <br><br>
            <div class="container">
                <div class="row" id="search-result"></div>
            </div>
        </div>
    </div>
</article>


<div class="player">
    <iframe id="iframe-player" width="100%" height="55" src="" frameborder="0" scrolling="no"></iframe>
</div>
<?php get_footer(); ?>


<script>

    jQuery(document).ready(function () {

        $("#search-input").keypress(function (event) {
            if (event.which == 13) {
                event.preventDefault();
            }

            $("#search-result").load("http://spokennow.com/index.php/search/?keyword=" + $(this).val());
        });

    });

    function selectShow(title, author, category, link, bg) {
        $("#show_title").html(title);
        $("#author").html(author);
        $("#category").html(category);

        $('#iframe-player').attr('src', "https://www.iradeo.com/station/embed/" + link);
        $('#hero-image').css('background-image', 'url(' + bg + ')');

    }

</script>
