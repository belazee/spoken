	<section class="footer">
		<div class="container">
			<div class="brief row pt-gutter pb-gutter">
				<div class="col-sm-9">
					<div class="logo pull-left">
						<b class="footer-logo cover"></b>
					</div>

					<div class="intro no-overflow pl-gutter">
						<div class="row">
							<div class="col-sm-6">
								Spoken is a new place for how audio is to be heard on internet. 
								Its measure of success is not based on views it has.
							</div>
							<div class="col-sm-6">
								Instead, it is based on how many points of view its carries.
								It is simply, the best way for people's views to be heard, understood, and spoken.
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3 social text-right">
					<a href="#" class="twitter">
						<i class="zmdi zmdi-twitter"></i>
					</a>

					<a href="#" class="facebook">
						<i class="zmdi zmdi-facebook"></i>
					</a>

					<a href="#" class="google">
						<i class="zmdi zmdi-google-plus"></i>
					</a>
				</div>
			</div>
		</div>

		<hr class="ma-0">

		<div class="copyright container">
			<div class="row pt-20 pb-20">
				<div class="col-sm-6">
					Copyright 2016 Spoken Asia Sdn Bhd. 
					<br class="visible-xs">
					All right reserved.
				</div>
				<div class="col-sm-6 text-right">
					<a href="#">
						Privacy Policy
					</a>

					<a href="#">
						Terms and Conditions
					</a>
				</div>
			</div>
		</div>
	</section>
</div><!--end .master > see _theme-head for references-->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/jquery.scrollbar.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/theme.js"></script>
</body>
</html>