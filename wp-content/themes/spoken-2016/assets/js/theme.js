$(function() {

	// Datepicker
	$('.form-datepicker').datepicker({
		format: 'dd/mm/yyyy'
	});

	// Toolbar's sidebar toggle
	$('.navbar-toggle-sidebar').on('click', function() {
		$('html').toggleClass('show-sidebar');
	});

	// Toolbar's search form toggle
	$('.navbar-toggle-search').on('click', function() {
		$('html').toggleClass('show-search');
	});

	// Modal login's stage
	$('.stage-link').on('click', function() {
		$("#theatre").removeClass (function (index, css) {
		    return (css.match (/(^|\s)show-stage-\S+/g) || []).join(' ');
		});

 		$('#theatre').addClass($(this).attr("data-stage"));
	});

	// Toolbar's search form toggle
	$('.card').on('click', function() {
		$(this).toggleClass('active');
	});
});


$(document).ready(function() {
	jQuery('.scrollbar-basic').scrollbar();

	jQuery('.scrollbar-external').scrollbar({
        "autoScrollSize": false,
        "scrollx": $('.external-scroll_x'),
        "scrolly": $('.external-scroll_y')
    });
});