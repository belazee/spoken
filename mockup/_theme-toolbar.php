<nav class="toolbar navbar navbar-default mb-0 pt-10 pb-10">
	<div class="container">
		<div class="navbar-header">
			<div type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
				<i class="zmdi zmdi-menu"></i>
			</div>

			<a class="navbar-brand cover-contain text-600 visible-lg visible-md" href="index.php">
				Spoken
			</a>

			<b class="navbar-brand cover-contain text-600 visible-sm visible-xs navbar-toggle-sidebar">
				Spoken
			</b>
		</div>

		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#">Discover</a>
				</li>
				<li class="navbar-login">
					<a href="#" class="text-400" data-toggle="modal" data-target="#modal-login" style="padding-left: 0; padding-right: 0;">
						Sign In
					</a>
				</li>
				<li class="navbar-user has-divider hidden-sms hidden-xs">
					<div class="navbar-form pa-0">
						<a href="#" class="btn btn-success text-400 pl-20 pr-20" data-toggle="modal" data-target="#modal-login">
							Register
						</a>
					</div>
				</li>

				<li class="navbar-user has-divider">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="img/mesut.jpg" width="30" height="30">
					</a>

					<ul class="dropdown-menu fly-from-right">
						<li><a href="#">Manage Account</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Sign Out</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>

<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-close stage-link" data-stage="show-stage-1" data-dismiss="modal">
				<i class="zmdi zmdi-close"></i>
			</div>

			<div class="clearfix">
				<div class="section hidden-xs">
					<div class="page row-table">
						<div class="col-cell">
							<h4>Why sign up?</h4>
							<hr>
							<ul class="ml-0 pl-20">
								<li>Save time filling forms</li>
								<li>Access your recent searches</li>
								<li>Track shortlisted, messaged properties</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="section">
					<div id="theatre" class="theatre show-stage-1">
						<div class="stage stage-1">
							<div class="page row-table">
								<div class="col-cell">
									<a href="#" class="btn btn-facebook btn-block">
										<i class="zmdi zmdi-facebook-box mr-10"></i>Continue with Facebook
									</a>

									<a href="#" class="btn btn-google btn-block">
										<i class="zmdi zmdi-google-glass mr-10"></i>Continue with Google+
									</a>

									<div class="text-muted mt-20 mb-20 text-center">Or use your email address</div>

									<div class="form-group form-clamp">
										<div class="form-group"><input type="email" class="form-control" placeholder="Email address"></div>
										<div class="form-group"><input type="password" class="form-control" placeholder="Password"></div>
									</div>

									<a href="#" class="btn btn-success btn-block">
										Login
									</a>

									<a href="javascript:void(0);" class="btn btn-link btn-block stage-link"  data-stage="show-stage-2">
										Forgot Password
									</a>

									<hr>

									<div class="text-center text-ssm text-muted text-uppercases mb-10">
										New to Spoken?

										<a href="javascript:void(0);" class="stage-link text-500" data-stage="show-stage-3">
											Sign Up
										</a>
									</div>
								</div>
							</div>
						</div>

						<div class="stage stage-2">
							<div class="page row-table">
								<div class="table-row" style="height: 50px;">
									<div class="col-cell pb-20">
										<a href="#" class="stage-link" data-stage="show-stage-1" style="font-size: 20px; line-height: 20px;">
											<i class="zmdi zmdi-arrow-left"></i>
										</a>

										<b style="line-height: 20px;">Reset Password</b>
									</div>
								</div>
								<div class="table-row cells-top">
									<div class="col-cell">
										<div class="mb-10">
											Enter the email address associated with your account, and we'll email you a link to reset your password.
										</div>

										<div class="form-group">
											<input type="email" class="form-control" placeholder="Email address">
										</div>

										<div class="form-group">
											<button class="btn btn-primary btn-block">Send Reset</button>
											<button class="btn btn-link btn-block stage-link" data-stage="show-stage-1">Back to Login</button>
										</div>
									</div>
								</div>

								<div class="table-row" style="height: 50px;">
									<div class="col-cell text-center">
										<hr>

										<div class="text-center text-ssm text-muted text-uppercases mb-10">
											New to Spoken?

											<a href="javascript:void(0);" class="stage-link text-500" data-stage="show-stage-3">
												Sign Up
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="stage stage-3">
							<div class="page row-table">
								<div class="table-row cells-top">
									<div class="col-cell">
										<div class="text-center">
											Register with your social account
										</div>
										<div class="mt-15 row row-sm">
											<div class="column col-xs-6">
												<a href="#" class="btn btn-block btn-facebook">
													<i class="zmdi zmdi-facebook-box mr-5"></i>
													Facebook
												</a>
											</div>
											<div class="column col-xs-6">
												<a href="#" class="btn btn-block btn-google">
													<i class="zmdi zmdi-google-glass mr-5"></i>
													Gmail
												</a>
											</div>
										</div>

										<div class="row-table text-xs text-muted text-center">
											<div class="col-cell pr-10"><hr></div>
											<div class="col-cell cell-tight">
												OR
											</div>
											<div class="col-cell pl-10"><hr></div>
										</div>

										<div class="form-group form-clamp">
											<div class="form-group has-success">
												<input type="text" class="form-control" placeholder="First name">
											</div>

											<div class="form-group has-error">
												<input type="text" class="form-control" placeholder="Last name">
											</div>
										</div>

										<div class="form-group form-clamp">
											<div class="form-group">
												<input type="email" class="form-control" placeholder="Email address">
											</div>

											<div class="form-group">
												<input type="text" class="form-control" placeholder="Phone Number">
											</div>
										</div>

										<div class="form-group">
											<input type="password" class="form-control" placeholder="Password">
										</div>

										<div class="form-action">
											<button class="btn btn-primary btn-block">Sign Up</button>
											<div class="text-xs text-center text-muted mt-10 mr-5 ml-5">
												By clicking Sign Up, you agree to Spoken's Terms of Use and Privacy Policy.
											</div>
										</div>
									</div>
								</div>

								<div class="table-row" style="height: 50px;">
									<div class="col-cell text-center">
										<hr>

										<div class="text-center text-ssm text-muted mb-10">
											Already have an account?

											<a href="javascript:void(0)" class="text-500 stage-link" data-stage="show-stage-1">
												Login
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include '_theme-sidebar.php'; ?>